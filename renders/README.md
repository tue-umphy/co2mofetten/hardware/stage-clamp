# Rendered Parameter Sets

These are the preconfigured parameter sets shipped with the design. You can select them in the Customizer.

# 50mm Clamp with Vertical Slot for Radiation Shield

![50mm Clamp with Vertical Slot for Radiation Shield](renders/50mm-Clamp-with-Vertical-Slot-for-Radiation-Shield.gif)

# 50mm Clamp with Loop

![50mm Clamp with Loop](renders/50mm-Clamp-with-Loop.gif)

# 50mm Clamp with Hex Screw Hole

![50mm Clamp with Hex Screw Hole](renders/50mm-Clamp-with-Hex-Screw-Hole.gif)

# 40mm Thin Clamp with Hex Screw Hole

![40mm Thin Clamp with Hex Screw Hole](renders/40mm-Thin-Clamp-with-Hex-Screw-Hole.gif)

# 50mm Clamp with 45° Plug

![50mm Clamp with 45° Plug](renders/50mm-Clamp-with-45-Plug.gif)

# 50mm Clamp with 45° Slot

![50mm Clamp with 45° Slot](renders/50mm-Clamp-with-45-Slot.gif)

# 50mm Clamp with Vertical Slot

![50mm Clamp with Vertical Slot](renders/50mm-Clamp-with-Vertical-Slot.gif)

# 50mm Clamp for Hensel Box 900X

![50mm Clamp for Hensel Box 900X](renders/50mm-Clamp-for-Hensel-Box-900X.gif)

# 60mm Clamp for Hensel Box 900X

![60mm Clamp for Hensel Box 900X](renders/60mm-Clamp-for-Hensel-Box-900X.gif)

# 14mm Clamp for Hensel Box 900X

![14mm Clamp for Hensel Box 900X](renders/14mm-Clamp-for-Hensel-Box-900X.gif)

# 14mm Clamp with Vertical Slot for Radiation Shield

![14mm Clamp with Vertical Slot for Radiation Shield](renders/14mm-Clamp-with-Vertical-Slot-for-Radiation-Shield.gif)

# 16mm Clamp with Vertical Slot for Radiation Shield

![16mm Clamp with Vertical Slot for Radiation Shield](renders/16mm-Clamp-with-Vertical-Slot-for-Radiation-Shield.gif)

# 16mm Clamp for Hensel Box 900X

![16mm Clamp for Hensel Box 900X](renders/16mm-Clamp-for-Hensel-Box-900X.gif)

# 16mm Clamp with 45° Slot for Radiation Shield

![16mm Clamp with 45° Slot for Radiation Shield](renders/16mm-Clamp-with-45-Slot-for-Radiation-Shield.gif)

# 16mm Clamp with Horizontal Slot for Radiation Shield

![16mm Clamp with Horizontal Slot for Radiation Shield](renders/16mm-Clamp-with-Horizontal-Slot-for-Radiation-Shield.gif)